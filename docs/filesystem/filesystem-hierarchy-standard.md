In short : FHS

````bash
bin     Essential command binaries     in single user mode
boot  Static files of the boot loader
dev   Devices files / nodes
etc    Host-specific system configuration
lib     Essential shared libraries and kernel modules /used by /bin /sbin)
media   Mount point for removable media
mnt      Mount point for mounting a filesystem temporarily
opt       Add-on application software packages
run      Data relevant to running processes
sbin    Essential system binaries
srv      Data for services provided by this system (Seldom used)
tmp     Temporary files
usr      Secondary hierarchy (unix system resources)
var      Variable data

proc    Virtual pseudo-filesystem giving info about sys & processes
sys      Virtual pseudo-filesystem giving info about sys & processes, part of Unified Device Model
root     Home directory for the root user

/boot    2 essentials files : vmlinuz : compressed linux kernel
                                         initramfs: mounted before the real root fs is available

````
