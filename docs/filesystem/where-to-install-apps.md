# Where to install applications

````bash
/usr:          all sys-wide, ro files installed by the OS
/usr/local:    sys-wide, ro files installed by local admin
/opt:          an atrocity meant for sys-wide, ro self-contained soft
~/.local:      per-user counterpart of /usr/local
~/.local/opt:  per-user counterpart of /opt
````
