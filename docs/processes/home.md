# Processes

First user process on the system is **init** (process ID=1)

## Shared libraries (DLL)

Have extension **.so**

Display what shared libraries an executable requires:

```bash
ldd /usr/bin/vi
```

## Zombies

Process in *zombie* state: has terminated, but no other process has yet asked about its exit state.

## Limits

```bash
ulimit -a: display or reset of resource limits
```

For percistants changes: */etc/security/limits.conf*

## Kill

list of signals:

```bash
kill -l
```

kill all process with a given name:

```bash
killall bash
```

Using pkill, will match name returned by `ps` command:

```bash
pkill [-signal][options][pattern]
pkill -u libby foobar
```

## Nice

Niceness: -20 (the highest priority) to +19 (the lowest priority)

lower priority of process 123:

```bash
renice +5 123
```
