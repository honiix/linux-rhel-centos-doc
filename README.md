# linux-rhel-centos-doc

Things I learned about managing Redhat and Centos servers. With many example of useful command that you don't find in the man pages right away.

## Docsify

Documentation created with [Docsify](git@gitlab.com:honiix/linux-rhel-centos-doc.git)

## Installed Plugins

* [Full text search](https://docsify.js.org/#/plugins?id=full-text-search)
* [Emoji](https://docsify.js.org/#/plugins?id=emoji)
* [Copy to clipboard](https://docsify.js.org/#/plugins?id=copy-to-clipboard)
* [Docsify-tabs](https://docsify.js.org/#/plugins?id=tabs)


## Todo

-[] Before spending to much time on this... evaluation cheat.sh
