* **Filesystem**

  * [Filesystem hierarchy standard](/filesystem/filesystem-hierarchy-standard.md)
  * [Where to install apps](/filesystem/where-to-install-apps.md)

* [Processes](/processes/home.md)
